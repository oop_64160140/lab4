import java.util.Scanner;

public class MyApp {
    public static void main(String[] args) {
        printWelcome();
        while(true){
            printMenu();
        int choice = inputChoice();
        switch (choice) {
            case 1:
                printHelloWorldNTime();
                break;
            case 2:
                addTwoNumbers();
                break;
            case 3:
                exitProgram();
                break;
            }
        }
    }


    private static void exitProgram() {
        System.out.println("Bye!!");
        System.exit(0);
    }

    private static void addTwoNumbers() {
        int first = inputFirst();
        int second = inputSecond();
        int result = add(first,second);
        printResult(result);
    }

    private static void printResult(int result) {
        System.out.println("Result = " + result);
    }


    private static int add(int first, int second) {
        int result = first + second;
        return result;
    }


    private static int inputSecond() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input second number: ");
        int second = sc.nextInt();
        return second;
    }


    private static int inputFirst() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        int first = sc.nextInt();
        return first;
    }


    private static void printHelloWorldNTime() {
        int time = inputTime();
        printHello(time);
        
    }

    private static void printHello(int time) {
        for(int i=0; i<time; i++){
            System.out.println("Hello World");
        }
    }


    private static int inputTime() {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input time: ");
            int time = sc.nextInt();
            if(time>=1){
                return time;
            } else{
                System.out.println("Error: Please input positive number!!!!");
            }
        }
    }


    private static int inputChoice() {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input choice: ");
            int choice = sc.nextInt();
            if(choice>=1 && choice<=3){
                return choice;
            } else{
                System.out.println("Error: Please input number between 1-3!!!!");
            }
        }
    }

    private static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    private static void printWelcome() {
        System.out.println("Welcome to My app!!!");
    }
}
